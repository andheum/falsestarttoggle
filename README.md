# False Start Toggle #

### About ###
This simple repo encapsulates a Grasshopper assembly containing a single object - a variant on the native `GH_BooleanToggle` object that resets itself to false on file open, so as to avoid accidentally triggering a simulation, for instance.

### How it works ###

The class inherits from the native `GH_BooleanToggle` and then overrides critical information (Name, Description, ComponentGUID) to avoid conflicts. The functionality change is achieved by overriding the "Read" method, which is called on deserialization from file or clipboard, and explicitly setting the value to `false` instead of the deserialized value.

### Credits ###
**Developer**: Andrew Heumann

**Concept**: Mathias Sønderskov Nielsen

**Custom Attributes Code** David Rutten