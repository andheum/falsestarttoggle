﻿using System.Drawing;
using Grasshopper;
using Grasshopper.GUI;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;

namespace FalseStartToggle
{
    //Custom attributes class for blue appearance
    public class GH_FalseStartToggleAttributes : GH_BooleanToggleAttributes
    {
        private RectangleF m_button;

        private RectangleF m_name;


        public GH_FalseStartToggleAttributes(GH_BooleanToggle owner) : base(owner)
        {
        }

        protected override void Layout()
        {
            Pivot = GH_Convert.ToPoint(Pivot);
            var tagWidth = GH_FontServer.StringWidth(Owner.NickName, GH_FontServer.StandardAdjusted);
            const int boxWidth = 50;
            tagWidth += 16;
            m_name = new RectangleF(Pivot, new Size(tagWidth, 22));
            m_button = new RectangleF(m_name.Right, m_name.Top, boxWidth, m_name.Height);
            Bounds = RectangleF.Union(m_name, m_button);
            m_name.Inflate(-2f, -2f);
            m_button.Inflate(-2f, -2f);
            //have to call base layout or else click interaction breaks
            base.Layout();
        }

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            if (channel != GH_CanvasChannel.Objects) return;
            var viewport = canvas.Viewport;
            var bounds = Bounds;
            var isVisible = viewport.IsVisible(ref bounds, 10f);
            Bounds = bounds;
            if (!isVisible)
            {
                return;
            }
            var style = GH_CapsuleRenderEngine.GetImpliedStyle(GH_Palette.Blue, Selected, Owner.Locked, true);
            var capBounds = GH_Capsule.CreateCapsule(Bounds, GH_Palette.Blue);
            capBounds.Font = GH_FontServer.StandardAdjusted;
            capBounds.AddOutputGrip(OutputGrip.Y);
            capBounds.Render(graphics, Selected, Owner.Locked, true);
            capBounds.Dispose();
            var alpha = GH_Canvas.ZoomFadeLow;
            if (alpha > 0 && Owner.NickName.Length > 0)
            {
                var nameFill = new SolidBrush(Color.FromArgb(alpha, style.Text));
                graphics.DrawString(Owner.NickName, GH_FontServer.StandardAdjusted, nameFill, m_name,
                    GH_TextRenderingConstants.CenterCenter);
                nameFill.Dispose();
            }
            const GH_Palette palButton = GH_Palette.Black;
            style = GH_Skin.palette_black_standard;
            var capButton = GH_Capsule.CreateCapsule(m_button, palButton, 1, 0);
            capButton.TextOrientation = GH_Orientation.horizontal_center;
            capButton.Font = GH_FontServer.ConsoleAdjusted;
            capButton.Text = Utility.TernaryIf(Owner.Value, "True", "False");
            capButton.Render(graphics, false, Owner.Locked, false);
            capButton.Dispose();
        }
    }
}