﻿using System;
using System.Drawing;
using FalseStartToggle.Properties;
using Grasshopper.Kernel;

namespace FalseStartToggle
{
    public class FalseStartToggleInfo : GH_AssemblyInfo
    {
        public override string Name => "FalseStartToggle";

        public override Bitmap Icon => Resources.FalseStartToggle;
        public override string Description => "A toggle that's always false when you open the file";

        public override Guid Id => new Guid("9bb830ad-1799-419e-8df4-c477e9c6f0db");

        public override string AuthorName => "Andrew Heumann";
        public override string AuthorContact => "andrew@heumann.com";
    }
}