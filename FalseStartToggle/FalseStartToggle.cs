﻿using System;
using System.Drawing;
using FalseStartToggle.Properties;
using GH_IO.Serialization;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;

namespace FalseStartToggle
{
    public class FalseStartToggle : GH_BooleanToggle
    {
        public FalseStartToggle()
        {
            //update name and description
            Name = "False Start Toggle";
            Description = "Just like a normal Boolean Toggle, except it always reverts to \"False\" on file open.";
        }

        //Must override guid to avoid conflicts
        public override Guid ComponentGuid => new Guid("{843FAD0D-8404-4AD3-961A-37A210182F70}");


        public override GH_Exposure Exposure => GH_Exposure.quarternary;

        protected override Bitmap Icon => Resources.FalseStartToggle;


        public override bool Read(GH_IReader reader)
        {
            // This is where saved properties are read in from the serialized data, on file open or copy/paste.
            // Instead of using the value loaded from the xml, this just overrides to false.
            var result = base.Read(reader);
            Value = false;
            return result;
        }

        public override void CreateAttributes()
        {
            //Change appearance so it's blue :)
            m_attributes = new GH_FalseStartToggleAttributes(this);
        }
    }
}